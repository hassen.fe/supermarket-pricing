package com.supermarket.pricing.kata;

import static org.junit.Assert.assertEquals;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;

import com.supermarket.pricing.kata.dao.Discount;
import com.supermarket.pricing.kata.dao.UnitPrice;
import com.supermarket.pricing.kata.dao.WeightPrice;
import com.supermarket.pricing.kata.dao.WeightUnit;
import com.supermarket.pricing.kata.service.PricingService;

public class PricingServiceTest {

    private UnitPrice unitPrice;
    private WeightPrice weightPrice;
    private Discount threeForDollar, sixForOneAndHalf, buyTwoGetOneFree;
    private Set<Discount> discounts;

    @Before
    public void setUp() throws Exception {
        unitPrice = new UnitPrice(0.65f);
        weightPrice = new WeightPrice(1.99f, WeightUnit.POUND);
        // one unit = 0.5$
        threeForDollar = new Discount(1.0f, 3f);
        buyTwoGetOneFree = new Discount(1.0f, 3f);
        sixForOneAndHalf = new Discount(1.5f, 6f);
        discounts = Stream.of(threeForDollar, sixForOneAndHalf).collect(Collectors.toSet());
    }

    @Test
    public void getRawPrice() throws Exception {
        assertEquals("a unit item costs $0.65", 0.65f, PricingService.getRawPrice(unitPrice, 1), Double.MIN_VALUE);
        assertEquals("$1.99/pound (so what does 4 ounces cost?)", 1.99f / 4f, PricingService.getRawPrice(weightPrice, 4.0f, WeightUnit.OUNCE), Double.MIN_VALUE);
    }


    @Test
    public void getDiscountsPriceTest() throws Exception {
        
    	//UNIT PRICE
    	assertEquals("Three units costs 1 Dollar: threeForOne discount selected",
                1f, PricingService.getDiscountPrice(new UnitPrice(0.5f), 3, threeForDollar), Double.MIN_VALUE);
        
    	  assertEquals("Four units costs 1.5 Dollars: 1 Dollar (threeForOne discount) + 0.5 Dollar ( 1 units) ",
                  1.5f, PricingService.getDiscountPrice(new UnitPrice(0.5f), 4, threeForDollar), Double.MIN_VALUE);
    	
        assertEquals("Five units costs 2 Dollars: 1 Dollar (threeForOne discount) + 1 Dollar ( 2 units) ",
                2f, PricingService.getDiscountPrice(new UnitPrice(0.5f), 5, threeForDollar), Double.MIN_VALUE);

        
        assertEquals("Six units costs 2 Dollars: 1 Dollar (buyTwoGetOneFree discount) +  1 Dollar (buyTwoGetOneFree discount)",
                2f, PricingService.getDiscountPrice(new UnitPrice(0.5f), 6, buyTwoGetOneFree), Double.MIN_VALUE);
        
        //WEIGHT PRICE
        assertEquals("32 Ounce costs 1 Dollars: 32 Ounce equals 2 Pounds, each costs 0.5 Dollar (No discount selected)",
                1f, PricingService.getDiscountPrice(new WeightPrice(0.5f, WeightUnit.POUND), 32f, WeightUnit.OUNCE, threeForDollar), Double.MIN_VALUE);
        assertEquals("48 Ounce costs 1 Dollar: 48 Ounce equals 3 Pounds, the threeForOne discount selected",
                1f, PricingService.getDiscountPrice(new WeightPrice(0.5f, WeightUnit.POUND), 48f, WeightUnit.OUNCE, threeForDollar), Double.MIN_VALUE);
        assertEquals("96 Ounce costs 2 Dollar: 96 Ounce equals 6 Pounds, the buyTwoGetOneFree discount selected Twice",
                2f, PricingService.getDiscountPrice(new WeightPrice(0.5f, WeightUnit.POUND), 96f, WeightUnit.OUNCE, buyTwoGetOneFree), Double.MIN_VALUE);

    }


   /**
    * TEST WITH LIST OF DISCOUTS FOR THE SAME PRODUCT
    * list of discounts = {sixForOneAndHalf , threeForOne}
    * @throws Exception
    */
    @Test
    public void getTheMinPriceTest() throws Exception {
    	
    	//UNIT PRICE
        assertEquals("The best price for 5 units is 2 Dollars: 1 Dollar (threeForOne discount) + 1 Dollar ( 2 units) ",
                2f, PricingService.getTheMinPrice(new UnitPrice(0.5f), 5, discounts), Double.MIN_VALUE);
        assertEquals("The best price for 3 units is 1 Dollar: threeForOne discount selected",
                1f, PricingService.getTheMinPrice(new UnitPrice(0.5f), 3, discounts), Double.MIN_VALUE);
        assertEquals("The best price for 6 units is 1.5 Dollars: sixForOneAndHalf discount selected",
                1.5f, PricingService.getTheMinPrice(new UnitPrice(0.5f), 6, discounts), Double.MIN_VALUE);

        //WEIGHT PRICE
        assertEquals("The best price for 32 ONCES is 2 Dollars: 32 Ounce equals 2 Pounds, each costs 1 Dollar (No discount selected)",
                2f, PricingService.getTheMinPrice(new WeightPrice(1f, WeightUnit.POUND), 32f, WeightUnit.OUNCE, discounts), Double.MIN_VALUE);
        assertEquals("The best price for 48 ONCES is 1 Dollar: 48 Ounce equals 3 Pounds, the threeForOne discount selected",
                1f, PricingService.getTheMinPrice(new WeightPrice(1f, WeightUnit.POUND), 48f, WeightUnit.OUNCE, discounts), Double.MIN_VALUE);
        assertEquals("The best price for 96 ONCES is 1.5 Dollar: 96 Ounce equals 6 Pounds, the sixForOneAndHalf discount selected",
                1.5f, PricingService.getTheMinPrice(new WeightPrice(1f, WeightUnit.POUND), 96f, WeightUnit.OUNCE, discounts), Double.MIN_VALUE);


    }


}