package com.supermarket.pricing.kata;

import org.junit.Test;

import com.supermarket.pricing.kata.Exception.NotAllowedConversionException;
import com.supermarket.pricing.kata.service.WeightService;

import java.util.function.Function;

import static com.supermarket.pricing.kata.dao.WeightUnit.*;
import static org.junit.Assert.assertEquals;

public class WeightServiceTest {

    @Test
    public void getConvertor() throws Exception {
        Function<Float, Float> poundToOunce=WeightService.getConvertor(POUND, OUNCE);
        assertEquals("One Pound equals 16 Ounces", 16.0f,poundToOunce.apply(1.0f), Double.MIN_VALUE);

        Function<Float, Float> ounceToPound=WeightService.getConvertor(OUNCE, POUND);
        assertEquals("16 Ounce equals 1 Pound", 1.0f,ounceToPound.apply(16.0f), Double.MIN_VALUE);

        Function<Float, Float> poundToPound=WeightService.getConvertor(POUND, POUND);
        assertEquals("Convert one Pound return one Pound", 1.0f,poundToPound.apply(1.0f), Double.MIN_VALUE);



    }
    @Test(expected = NullPointerException.class)
    public void getConvertorWithException() throws Exception, NotAllowedConversionException {


        Function<Float, Float> nullToOunce=WeightService.getConvertor(null, OUNCE);
        assertEquals("nullPointerException, the input unit is null", 16.0f,nullToOunce.apply(1.0f), Double.MIN_VALUE);

        Function<Float, Float> poundToNull=WeightService.getConvertor(POUND, null);
        assertEquals("nullPointerException, the output unit is null", 16.0f,poundToNull.apply(1.0f), Double.MIN_VALUE);

    }


}