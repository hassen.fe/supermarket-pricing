package com.supermarket.pricing.kata.service;

import static com.supermarket.pricing.kata.service.PowerSetService.powerSet;
import static com.supermarket.pricing.kata.service.WeightService.getConvertor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.supermarket.pricing.kata.Exception.NotAllowedConversionException;
import com.supermarket.pricing.kata.dao.Discount;
import com.supermarket.pricing.kata.dao.UnitPrice;
import com.supermarket.pricing.kata.dao.WeightPrice;
import com.supermarket.pricing.kata.dao.WeightUnit;

public class PricingService {

	/**
	 * Computes the weight price;
	 *
	 * @param price    The weight price
	 * @param quantity the Quantity
	 * @param unit     the unit of the quantity
	 * @return the price in dollar currency;
	 */
	public static float getRawPrice(WeightPrice price, Float quantity, WeightUnit unit)
			throws NotAllowedConversionException {
		return price.getValue() * getConvertor(unit, price.getUnit()).apply(quantity);
	}

	/**
	 * Computes the unit price;
	 *
	 * @param price    The unit price
	 * @param quantity the Quantity
	 * @return the price in dollar currency;
	 */
	public static float getRawPrice(UnitPrice price, Integer quantity) {
		return price.getValue() * quantity;
	}

	/**
	 * Computes the weight price with applying the given discount;
	 *
	 * @param price    the unit price
	 * @param quantity the Quantity
	 * @param unit     the unit of the the Quantity
	 * @param discount the discount to be applied
	 * @return the price in dollar currency;
	 */

	public static float getDiscountPrice(WeightPrice price, Float quantity, WeightUnit unit, Discount discount)
			throws NotAllowedConversionException {
		Float realQuantity = getConvertor(unit, price.getUnit()).apply(quantity);
		if (realQuantity < discount.getQuantity()) {
			return getRawPrice(price, quantity, unit);
		}

		int div = (int) (realQuantity / discount.getQuantity());
		float rest = (int) (realQuantity % discount.getQuantity());
		return div * discount.getPrice() + rest * price.getValue();
	}

	/**
	 * Computes the unit price with applying the given discount;
	 *
	 * @param price    the unit price
	 * @param quantity the Quantity
	 * @param discount the discount to be applied
	 * @return the price in dollar currency;
	 */

	public static float getDiscountPrice(UnitPrice price, Integer quantity, Discount discount) {
		if (quantity < discount.getQuantity()) {
			return getRawPrice(price, quantity);
		}
		int div = (int) (quantity / discount.getQuantity());
		int rest = (int) (quantity % discount.getQuantity());
		return div * discount.getPrice() + rest * price.getValue();
	}

	/**
	 * Computes the price using the given collection of discounts;
	 *
	 * @param price    The unit price
	 * @param quantity the Quantity
	 * @return the minimum price in dollar currency;
	 */
	public static float getDiscountsPrice(UnitPrice price, Integer quantity, Set<Discount> discounts) {

		Float sum = 0f;
		// GET LIST OF DISCOUNTS FROM A SET ORDERED BY QUANTITY DESC
		List<Discount> listDiscounts = new ArrayList<Discount>(discounts);
		Comparator<Discount> comparator = Comparator.comparing(Discount::getQuantity); 
		Collections.sort(listDiscounts, comparator.reversed());
		
		int i = 0;
		while (!listDiscounts.isEmpty() && i < listDiscounts.size() && quantity > 0) {
			Discount discount = listDiscounts.get(i);
			if (quantity < discount.getQuantity()) {
				sum = getRawPrice(price, quantity);
				i++;
			} else {
				int div = (int) (quantity / discount.getQuantity());
				int rest = (int) (quantity % discount.getQuantity());
				Float priceDiv = discount.getPrice();
				if (rest == 0) {
					sum = div * priceDiv;
					quantity = 0;
				} else {
					listDiscounts = listDiscounts.subList(i + 1, listDiscounts.size());
					if (listDiscounts.isEmpty()) {
						sum = div * priceDiv + rest * price.getValue();
						quantity = 0;
					} else {
						// apply the remaining discounts on the rest
						sum = div * priceDiv + getDiscountsPrice(price, rest, new HashSet<>(listDiscounts));
					}
				}
				i++;
			}
		}

		return sum;
	}

	/**
	 * Computes the price using the given collection of discounts;
	 *
	 * @param price    The unit price
	 * @param quantity the Quantity
	 * @param unit     the unit of the quantity
	 * @return the minimum price in dollar currency;
	 */

	public static float getDiscountsPrice(WeightPrice price, Float quantity, WeightUnit unit, Set<Discount> discounts)
			throws NotAllowedConversionException {
		Float sum = 0f;
		// GET LIST OF DISCOUNTS FROM A SET ORDERED BY QUANTITY DESC
		List<Discount> listDiscounts = new ArrayList<Discount>(discounts);
		Comparator<Discount> comparator = Comparator.comparing(Discount::getQuantity); 
		Collections.sort(listDiscounts, comparator.reversed());
		int i = 0;

		while (!listDiscounts.isEmpty() && i < listDiscounts.size()  && quantity > 0) {
			Discount discount = listDiscounts.get(i);
			Float realQuantity = getConvertor(unit, price.getUnit()).apply(quantity);

			if (realQuantity < discount.getQuantity()) {
				sum = getRawPrice(price, quantity, unit);
				i++;
			} else {
				int div = (int) (realQuantity / discount.getQuantity());
				float rest = (float) (realQuantity % discount.getQuantity());

				Float priceDiv = discount.getPrice();
				if (rest == 0f) {
					sum = div * priceDiv;
					quantity = 0f;
				} else {
					listDiscounts = listDiscounts.subList(i + 1, listDiscounts.size());
					if (listDiscounts.isEmpty()) {
						sum = div * priceDiv + rest * price.getValue();
						quantity = 0f;
					} else {
						// apply the remaining discounts on the rest
						sum = div * priceDiv + getDiscountsPrice(price, rest, unit, new HashSet<>(listDiscounts));
					}

				}
				i++;
			}
		}

		return sum;
	}

	/**
	 * Computes the minimum global price by exploring all discounts combinations;
	 *
	 * @param price    The unit price
	 * @param quantity the Quantity
	 * @return the minimum price in dollar currency;
	 */
	public static Float getTheMinPrice(UnitPrice price, Integer quantity, Collection<Discount> discounts) {

		Set<Discount> discountsSet = new HashSet<Discount>(discounts);
		Set<Set<Discount>> subDiscountSets = powerSet(discountsSet);

		Float minPrice = 10000000000f;

		for (Set<Discount> discountSet : subDiscountSets) {
			if (!discountSet.isEmpty()) {

				Float discountsPrice = getDiscountsPrice(price, quantity, discountSet);
				minPrice = (discountsPrice < minPrice) ? discountsPrice : minPrice;
			}
		}
		return minPrice;

	}

	/**
	 * Computes the minimum global price by exploring all discounts combinations;
	 *
	 * @param price    The unit price
	 * @param quantity the Quantity
	 * @param unit     the unit of the quantity
	 * @return the minimum price in dollar currency;
	 */

	public static Float getTheMinPrice(WeightPrice price, Float quantity, WeightUnit unit,
			Collection<Discount> discounts) throws NotAllowedConversionException {
		Set<Discount> discountsSet = new HashSet<Discount>(discounts);
		Set<Set<Discount>> subDiscountSets = powerSet(discountsSet);

		Float minPrice = 10000000000f;

		for (Set<Discount> discountSet : subDiscountSets) {
			if (!discountSet.isEmpty()) {

				Float discountsPrice = getDiscountsPrice(price, quantity, unit, discountSet);
				minPrice = (discountsPrice < minPrice) ? discountsPrice : minPrice;
			}
		}
		return minPrice;
	}

}
