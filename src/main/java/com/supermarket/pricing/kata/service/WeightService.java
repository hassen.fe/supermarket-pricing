package com.supermarket.pricing.kata.service;



import java.util.function.Function;

import com.supermarket.pricing.kata.Exception.NotAllowedConversionException;
import com.supermarket.pricing.kata.dao.WeightUnit;

public class WeightService {

    public static Function<Float, Float> getConvertor(WeightUnit inputUnit, WeightUnit outputUnit) throws NotAllowedConversionException {
        if (inputUnit == null) {
            throw new NullPointerException("input unit shouldn't be null.");
        }
        if (outputUnit == null) {
            throw new NullPointerException("output unit shouldn't be null.");
        }
        if (outputUnit == inputUnit) {
            return weight -> weight;
        }
        switch (inputUnit) {
            case OUNCE:
                return weight -> weight / 16;
            case POUND:
                return weight -> weight * 16;
            default:
                throw new NotAllowedConversionException();
        }

    }

}
