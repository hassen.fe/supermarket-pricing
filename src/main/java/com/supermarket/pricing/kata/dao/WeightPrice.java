package com.supermarket.pricing.kata.dao;


public class WeightPrice extends PricingStrategy {

    private final WeightUnit unit;
    
    
    public WeightPrice(Float value, WeightUnit unit) {
        super(value);
        this.unit = unit;
    }

    public WeightUnit getUnit() {
        return unit;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeightPrice other = (WeightPrice) obj;
		if (unit != other.unit)
			return false;
		return true;
	}
	
    @Override
	public String toString() {
		return "WeightPrice [unit=" + unit + "]";
	}
    
}
