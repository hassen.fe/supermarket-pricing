package com.supermarket.pricing.kata.dao;

public abstract class PricingStrategy {

	
	/**
     * The price value
     */
    private final Float price;

    public PricingStrategy(Float value) {
        this.price = value;
    }
    
	public Float getValue() {
		return price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PricingStrategy other = (PricingStrategy) obj;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PricingStrategy [price=" + price + "]";
	}
	
	
    
}
