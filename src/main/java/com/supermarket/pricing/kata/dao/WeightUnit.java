package com.supermarket.pricing.kata.dao;

public enum WeightUnit {

    POUND,
    OUNCE;
}
